const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name: {
        first: String,
        last: String
    },
    age: {
        type: Number,
        min: 1,
        max: 100
    },
    sex: {
        type: String,
        default: 'Null',
        immutable: true
    },
    email: {
        type: String,
        minLength: 1,
        maxLength: 20,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid email')
            }
        }
    },
    eggs: {
        type: Number,
        min: [6, 'Must be at least 6, got {VALUE}'],
        max: [10, 'Must be less than 10, got {VALUE}']
    },
    drink: {
        type: String,
        // enum: ['coffee','tea']
        enum: { values: ['coffee', 'tea'], message: '{VALUE} is not supported' },
    },
    friends: [
        {
            name: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
            },
            friendAddress: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'friendAddress',
                // required: true
            }
        }
    ],
    address: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
    }
})

userSchema.virtual('fullname')
    .get(function () {
        return this.name.first + ' ' + this.name.last
    })
    .set(function (value) {
        var array = value.split(' ')
        this.name.first = array[0]
        this.name.last = array[1]
    })

userSchema.pre('save', function () {
    // console.log(this.eggs)
    this.eggs = this.eggs + 1
})


userSchema.post('save', function () {
    console.log('Post: ' + this.eggs)
})




const User = mongoose.model('User', userSchema)
module.exports = User
