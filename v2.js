const mongoose = require('mongoose')

const v1addressSchema = new mongoose.Schema({

    country: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Country'
    },
    street: {
        type: String,
        required: true,
        trim: true
    }

}, {
    timestamps: true
})

const v1Address = mongoose.model('v1Address', v1addressSchema)
module.exports = v1Address