const mongoose = require('mongoose');
const express = require('express')

const port = 3000
const app = express()

app.use(express.json())
mongoose.connect('mongodb://127.0.0.1:27017/work')

const Category = require('./category')
const SubCategory = require('./sub-category')
const Product = require('./product');
const subCategory = require('./sub-category');




app.get('/', async (req, res) => {


    // const category = await Category.create({
    //     category: 'Vehicle',
    // })
    // const category1 = await Category.create({
    //     category: 'Cloths',
    // })
    // const category2 = await Category.create({
    //     category: 'Drinks',
    // })

    // res.send(category)


    // const subCategory = await SubCategory.create({

    //     subCategoryName: 'soft Drinks',
    //     categoryId: '6203a50410104fda3efc523f'

    // })
    // const subCategory2 = await SubCategory.create({

    //     subCategoryName: 'Hard Drinks',
    //     categoryId: '6203a50410104fda3efc523f'

    // })

    // res.send(subCategory)


    const product = await Product.create({
        productName: 'Coco Cola',
        subCategoryId: '6204a3d3e16198a05e6a1bdf',
        categoryId: '6203a50410104fda3efc523d'
    })
    // const product1 = await Product.create({
    //     productName: 'Limca',
    //     subCategoryId: '6204a40c40d2f93197d2fdf7',
    //     categoryId: '6203a50410104fda3efc523f'
    // })
    // const product2 = await Product.create({
    //     productName: 'Cherry'
    // })
    res.send({ product })

})


app.get('/get', async (req, res) => {

    const data = await Category.aggregate([
        // {
        //     $match: {
        //         category: 'Mst',
        //     }
        // },
        {
            $lookup: {
                from: 'subs', // collection 2 name
                localField: 'subCategory', //field name is 1st table
                foreignField: '_id', //Primary key of second table
                // "pipeline": [
                //     { "$project": { "_id": 0, "__v": 0 } }
                // ],
                as: 'subCategory', // keep name same as localField name to remove an extra field
            },
        },
        {
            $unwind: {
                path: "$subCategory",
                preserveNullAndEmptyArrays: false,
            }
        },
        {
            $lookup: {
                from: 'products', // collection 2 name
                localField: 'subCategory.products', //field name is 1st table
                foreignField: '_id', //Primary key of second table
                // "pipeline": [
                //     { "$project": { "_id": 0, "__v": 0 } }
                // ],
                as: 'subCategory.products',
            },
        },

        // {
        //     $unwind: {
        //         path: "$subCategory.products",
        //         preserveNullAndEmptyArrays: false,
        //     }
        // },


        // {
        //     $group: {
        //         _id: {
        //             id: '$_id',
        //             category: '$category',
        //             subCategoryName: '$subCategory.subCategoryName',
        //             // products: '$subCategory.products'

        //         },
        //         // subCategory: { $push: '$subCategory' }


        //         // subCategoryName: { $first: '$subCategory.subCategoryName' },


        //         // 'subCategory.products': { $push: '$subCategory.products' }
        //     }
        // },

        // {
        //     $group: {
        //         _id: '$_id',
        //         category: { $first: '$category' },
        //         subCategory: { $push: '$subCategory' }
        //     }
        // },

        {
            $group: {
                _id: '$subCategory._id',
                categoryName: { $first: '$subCategoryName' },
            }
        },


        // {
        //     $unwind: {
        //         path: "$subCategory",
        //         preserveNullAndEmptyArrays: true,
        //     }
        // },
        // {
        //     $unwind: {
        //         path: "$subCategory.products",
        //         preserveNullAndEmptyArrays: true,
        //     }
        // },



        {
            $project: {
                // _id: 0,
                // sex: 0,
                // __v: 0,

                category: 1,
                products: 1,
                subCategory: 1,


                // subCategory: {
                //     products: {

                //         // $cond: {
                //         //     if: { $eq: [[], "$products"] },
                //         //     then: "$$REMOVE",
                //         //     else: "$subCategory.products"
                //         // }
                //         // $cond: {
                //         //     if: { $size: 0 },
                //         //     then: { $exists: false },
                //         //     else: { $exists: true },
                //         // }

                //     },
                // }

            }
        }

    ])
    res.send(data)



})



app.get('/get2', async (req, res) => {


    const data = await Category.aggregate([
        {
            "$lookup": {
                "from": "subs",
                "let": { "catId": "$_id" },
                "pipeline": [
                    { "$match": { "$expr": { "$eq": ["$_id", "$$catId"] } } },
                    {
                        "$lookup": {
                            "from": "products",
                            "let": { "productId": "$_id" },
                            "pipeline": [
                                { "$match": { "$expr": { "$eq": ["$_id", "$$productId"] } } }
                            ],
                            "as": "output"
                        }
                    }
                ],
                "as": "output"
            }
        },
        // { "$unwind": "$output" }
    ])

    res.send(data)

})





app.get('/yt', async (req, res) => {

    const data = await Category.aggregate([
        {
            $lookup: {
                from: 'subs',
                as: 'subCategory',
                let: { categoryid: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: ['$categoryId', '$$categoryid'] }
                        }
                    },
                    {
                        $lookup: {
                            from: 'products',
                            as: 'products',
                            let: { productid: '$_id' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: { $eq: ['$subCategoryId', '$$productid'] }

                                    }
                                }
                            ]

                        }
                    },
                    {
                        $unwind: {
                            path: '$products',
                        }

                    },
                    {
                        $group: {
                            _id: {
                                id: '$_id',
                                subCategory: '$subCategoryName'
                            },
                            products: { $push: '$products' }

                        }
                    }
                ],

            },
        },
        {
            $unwind: {
                path: '$subCategory',
                preserveNullAndEmptyArrays: false
            }

        },
        {
            $group: {
                _id: {
                    id: '$_id',
                    category: '$category'
                },
                subCategory: { $push: '$subCategory' }
            }
        }

    ])


    res.send(data)



})


// final working nested lookup

app.get('/yt2', async (req, res) => {

    const data = await Category.aggregate([
        {
            $lookup: {
                from: 'subs',
                as: 'subCategory',
                let: { categoryid: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: ['$categoryId', '$$categoryid'] }

                        }
                    },
                    {
                        $lookup: {
                            from: 'products',
                            as: 'products',
                            let: { productid: '$_id' },
                            pipeline: [
                                {
                                    $match:
                                    {
                                        $expr: { $eq: ['$subCategoryId', '$$productid'] }
                                    }
                                }
                            ]

                        }
                    },
                    {
                        $match: {
                            products: {
                                $exists: true,
                                $not: {
                                    $size: 0
                                }
                            }
                        }

                    }
                ],

            },
        }, {
            $match: {
                subCategory: {
                    $exists: true,
                    $not: {
                        $size: 0
                    }
                }
            }

        }
    ])

    res.send(data)

})








app.listen(port, (error) => {
    if (error) {
        return console.log('0Error starting server \n' + error)
    }
    console.log('Server is up and running on port: ' + port)
})