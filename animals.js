const mongoose = require('mongoose')


const animalSchema = new mongoose.Schema({
    name: {
        type: String
    }
})

animalSchema.set('autoIndex', false)
animalSchema.index({ _id: 100 }, { sparse: true })
const animal = mongoose.model('Animal', animalSchema)

animal.on('index', error => {
    // "_id index cannot be sparse"
    console.log(error.message);
});

module.exports = animal

