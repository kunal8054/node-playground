db.party.aggregate([
    {
        "$lookup": {
            "from": "address",
            "let": { "partyId": "$_id" },
            "pipeline": [
                { "$match": { "$expr": { "$eq": ["$party_id", "$$partyId"] } } },
                {
                    "$lookup": {
                        "from": "addressComment",
                        "let": { "addressId": "$_id" },
                        "pipeline": [
                            { "$match": { "$expr": { "$eq": ["$address_id", "$$addressId"] } } }
                        ],
                        "as": "address"
                    }
                }
            ],
            "as": "address"
        }
    },
    { "$unwind": "$address" }
])