const mongoose = require('mongoose')

const fileSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    age: {
        type: Number,
        required: true
    },

    avatar: {
        type: Buffer,
    }

})

module.exports = mongoose.model('file', fileSchema)