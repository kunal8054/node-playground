const mongoose = require('mongoose')

const v1Schema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    age: {
        type: Number,
        required: true
    },

    address: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'v1Address'
    }

})

module.exports = mongoose.model('v1', v1Schema)