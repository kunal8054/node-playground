const async = require('async')

// // using Arrays
// async.series([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'one')
//         }, 5000)
//     },
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'two')
//         }, 100)
//     },
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'twelve')
//         }, 1000)
//     },
// ], function (err, result) {
//     if (err) { return console.log(err) }
//     console.log(result)
// })


// Using Object
// async.series({
//     one: function (callback) {
//         setTimeout(function () {
//             callback(null, 1)
//         }, 1000)
//     },
//     two: function (callback) {
//         setTimeout(function () {
//             callback(null, 2)
//         }, 2000)
//     }
// }, function (err, result) {
//     if (err) { return console.log(err) }
//     console.log(result)
// })



// Using promises using object
// async.series({
//     one: function (callback) {
//         setTimeout(function () {
//             callback(null, 1)
//         }, 1000)
//     },
//     two: function (callback) {
//         setTimeout(function () {
//             callback(null, 2)
//         }, 2000)
//     }
// }).then(result => console.log(result))
//     .catch(err => console.log(err))



// Using promises using arrays -> sane as above


// using async await 

// async function run() {
//     try {

//         let result = await async.series({
//             one: function (callback) {
//                 setTimeout(function () {
//                     callback(null, 1)
//                 }, 1000)
//             },
//             two: function (callback) {
//                 setTimeout(function () {
//                     callback(null, 2)
//                 }, 2000)
//             }
//         })
//         console.log(result)

//     } catch (error) {
//         console.log(error)
//     }
// }
// run()



// Async parallel vs series

// async.parallel([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'P one');
//         }, 5000);
//     },
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'P two');
//         }, 5000);
//     }
// ], function (err, results) {
//     console.log(results);

// });


// series
// async.series([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'one');
//         }, 5000);
//     },
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'two');
//         }, 5000);
//     }
// ], function (err, results) {
//     console.log(results);
// });


// Waterfall
// async.waterfall([

//     function (callback) {
//         callback(null, 1, 2)
//     },
//     function (a, b, callback) {
//         callback(null, a + b)
//     },
//     function (c, callback) {
//         callback(null, c+4)
//     }

// ], function (err, result) {
//     if (err) return console.log(err)
//     console.log(result)
// })



//Using names functions
// async.waterfall([
//     fun1,
//     fun2,
//     fun3
// ], function (err, result) {
//     if (err) return console.log(err)
//     console.log(result)
// })


// function fun1(callback) {
//     callback(null, 1, 2)
// }

// function fun2(a, b, callback) {
//     callback(null, a + b)
// }

// function fun3(c, callback) {
//     callback(null, c + 4)
// }
