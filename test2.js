const mongoose = require('mongoose')

const test2Schema = new mongoose.Schema({
    dob: {
        type: String,
    }
})

module.exports = mongoose.model('test2', test2Schema)