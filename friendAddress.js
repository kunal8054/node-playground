const mongoose = require('mongoose')

const friendAddressSchema = new mongoose.Schema({

    local: {
        type: String,
        required: true
    },

}, {
    timestamps: true
})

const friendAddress = mongoose.model('friendAddress', friendAddressSchema)
module.exports = friendAddress