const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        trim: true
    },
    subCategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'sub'
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'category'
    }

})

module.exports = mongoose.model('product', productSchema)