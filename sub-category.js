const mongoose = require('mongoose')

const subSchema = new mongoose.Schema({
    subCategoryName: {
        type: String,
        trim: true,
        required: true
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'category'
    }
})

module.exports = mongoose.model('sub', subSchema)