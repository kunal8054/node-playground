# apply()

<br>

Method Reuse
With the apply() method, you can write a method that can be used on different objects.

    const person = {
      fullName: function() {
        return this.firstName + " " + this.lastName;
      }
    }

    const person1 = {
      firstName: "Mary",
      lastName: "Doe"
    }

    // This will return "Mary Doe":
    person.fullName.apply(person1);

<br>

### The Difference Between call() and apply()

The difference is:

The **call()** method takes arguments separately.

The **apply()** method takes arguments as an array.

<br>

    const person = {
    fullName: function(city, country) {
        return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
    }

    const person1 = {
    firstName:"John",
    lastName: "Doe"
    }

    person.fullName.apply(person1, ["Oslo", "Norway"]);

.

    Math.max(1,2,3);  // Will return 3

Since JavaScript arrays do not have a max() method, you can apply the Math.max() method instead.

    Math.max.apply(null, [1,2,3]); // Will also return 3

The first argument (null) does not matter.
